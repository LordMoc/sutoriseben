from django.urls import path
from . import views

app_name = 'rumah'

urlpatterns = [
    path('', views.index, name='index'),
]